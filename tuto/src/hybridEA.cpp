//-----------------------------------------------------------------------------
/** 
 *
 * memeticEA.cpp
 *
 * SV - 2021/11/29 - version 1
 *
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// the general include for eo

#include <eo>   // Evolving Object: fundation of EO, evolutionary algorithm
#include <mo>   // Moving Object: Local search, combinatorial optimization mainly
#include <ga.h> // Genetic (binary strings search space) algorithm part of EO 

//-----------------------------------------------------------------------------
// Fitness function
#include <eval/nkLandscapesEval.h> // nk landscapes problem (from problesm/eval/nkLandscapesEval.h)
#include <problems/eval/moNKlandscapesIncrEval.h> // evaluation of a neighbor for nk-landscapes problem (from mo/src/problems/eval/moNKlandscapesIncrEval.h)

//-----------------------------------------------------------------------------
// For Hybrid EA : maybe in the next update of EO
#include <eoCombinedMonOp.h>      // from the local directory src/utils
#include <eoProbabilisticMonOp.h> // from the local directory src/utils

//-----------------------------------------------------------------------------
// type of the representations
typedef double Fitness;              // Fitness type, maximization problem
typedef eoBit<Fitness> Indi;         // bit string with double fitness value
typedef moBitNeighbor<Fitness> Neighbor; // neighbor: one bit flip


int main(int argc, char** argv) {
    /* =========================================================
     *
     * Parameters
     *
     * ========================================================= */

    eoParser parser(argc, argv);

  	// random seed
    uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();

    // bit string size
    size_t n = parser.createParam<size_t>(20, "vecSize", "Problem dimension", 'n', "Problem").value();

    // non-linear degree (K)
    size_t k = parser.createParam<size_t>(1, "nonLinearDegree", "Non-linear degree of nk-landscapes", 'k', "Problem").value();

    // random seed of nk-landscape
    uint32_t seedNK = parser.createParam(time(0), "seedNK", "Random number seed of the nk-landscape instance", 'I', "Problem").value();

    // Population size
    uint32_t mu = parser.createParam(100, "popSize", "Population size", 'P', "Algorithm").value();

    // Tournament selection size
    unsigned int tSize = parser.createParam(2, "tournamentSize", "Size of tournament selection", 'T', "Algorithm").value();

    // Neighborhood selection strategy
    unsigned int selectionStrategy = parser.createParam(0, "pivot", "Pivot rule of the hill-climber. 0: best improvement, 1: first improvement", 'p', "Algorithm").value();

    // bit flip mutation rate per bit
    double mutationRate_perBit = parser.createParam(5.0 / n, "mrate", "Mutation rate per bit", 'm', "Algorithm").value();

    // mutation rate
    double mutationRate = parser.createParam(0.1 / mu, "mutationRate", "Mutation rate", 'M', "Algorithm").value();

    // xover rate
    double xoverRate = parser.createParam(0.8, "xoverRate", "CrossOver rate", 'X', "Algorithm").value();

    // stopping criterium based on time 
    time_t duration = parser.createParam(1, "time", "Time limit stopping criterium (number of seconds)", 't', "Algorithm").value();

    // stopping criterium based on number of evaluations
    uint32_t maxEval = parser.createParam(1000, "maxEval", "Number of evaluations stopping criterium", 'e', "Algorithm").value();

    // output file
    std::string fileOutName = parser.createParam(std::string("out.csv"), "output", "Output file name to report statistics", 'o', "Ouput").value();

    make_verbose(parser);
    make_help(parser);

    /* =========================================================
     *
     * Fitness function
     *
     * ========================================================= */

    // pre-define fitness function to maximize: nk-landscapes problem
    rng.reseed(seedNK);
    nkLandscapesEval<Indi> _eval(n, k);
    // to count the number of evalation
    eoEvalFuncCounter<Indi> eval(_eval, "neval");

    // Use it if there is no incremental evaluation: a neighbor is evaluated by the full evaluation of a solution
    moFullEvalByModif<Neighbor> _neighborEval(eval);  // to count the number of real evaluations (stopping criterium)

    // Incremental evaluation of the neighbor
    //moNKlandscapesIncrEval<Neighbor> _neighborEval(_eval);
    // to count the number of evalation
    moEvalCounter<Neighbor> neighborEval(_neighborEval);

    /* =========================================================
     *
     * Random seed
     *
     * ========================================================= */

    rng.reseed(seed);

    /* =========================================================
     *
     * Initialization
     *
     * ========================================================= */

    // initialisation of one single gene (bit)
    eoUniformGenerator<Indi::AtomType> genInit;

    // Initialisation of the solution (vector of bits)
    eoInitFixedLength<Indi> init(n, genInit);

    /* =========================================================
     *
     * Neighborhood
     *
     * ========================================================= */

    // Exploration of the neighborhood in random order of the neigbor's index:
    // each neighbor is visited only once
    moRndWithoutReplNeighborhood<Neighbor> rndNeighborhood(n);

    moOrderNeighborhood<Neighbor> orderNeighborhood(n);

    /* =========================================================
     *
     * continuator: stopping criterium
     *
     * ========================================================= */

    // time stopping criterium
    moTimeContinuator<Neighbor> timeContinuator(duration, false);

    // maximum number of evaluations
    moFullEvalContinuator<Neighbor> evalContiuator(eval, maxEval, false);

    // multiple stopping criteria
    moCombinedContinuator<Neighbor> hcContinuator(timeContinuator);
    hcContinuator.add(evalContiuator);

    // Continuator for the EA
    eoEvalContinue<Indi> evalCont(eval, maxEval);

    eoTimeContinue<Indi> timeCont(duration);

    eoCombinedContinue<Indi> continuator(evalCont);
    continuator.add(timeCont);

    /* =========================================================
     *
     * basic local search
     *
     * ========================================================= */

    moLocalSearch<Neighbor> * hc;

    switch (selectionStrategy) {
        // best improvement
        case 0: hc = new moSimpleHC<Neighbor>(orderNeighborhood, eval, neighborEval, hcContinuator);
        break;
        // first improvement
        case 1: hc = new moFirstImprHC<Neighbor>(rndNeighborhood, eval, neighborEval, hcContinuator);
        break;
    }

    /* =========================================================
     *
     * Selection of EA
     *
     * ========================================================= */

    // Tournament selection: to select one individual
    eoDetTournamentSelect<Indi> selectOne(tSize);       
    
    // is now encapsulated in a eoSelectPerc to select a population (by default lambda = mu)
    eoSelectPerc<Indi> select(selectOne);

    /* =========================================================
     *
     * Variation operators
     *
     * ========================================================= */

    // standard bit-flip mutation for bitstring
    eoBitMutation<Indi>  bitOp(mutationRate_perBit);
    eoProbabilisticMonOp<Indi> bitMutation(bitOp, mutationRate);

    // combine with Hill-Climber: bit flip mutation, then hill-climber
    eoCombinedMonOp<Indi> mutation(bitMutation);
    mutation.add(*hc);

    // 1-point crossover for bitstring
    eo1PtBitXover<Indi> xover;

    // The operators are encapsulated into an eoTransform object
    eoSGATransform<Indi> variation(xover, xoverRate, mutation, 1.0);

    /* =========================================================
     *
     * Replacement
     *
     * ========================================================= */

    // (mu + lambda)-Evolution Strategy replacement
    eoPlusReplacement<Indi> replace;

    /* =========================================================
     *
     * Output some statistics
     *
     * ========================================================= */

    // checkpoint: substitute continuator to report some statistic, ouput, etc.
    eoCheckPoint<Indi> checkpoint(continuator);

    // output into a file: file name, separator, keep file, header
    eoFileMonitor monitor(fileOutName, " ", false, true);

    // add the monitor to the checkpoint to be processed
    checkpoint.add(monitor);

    // Create a counter parameter
    eoValueParam<unsigned> generationCounter(0, "iteration");
    // Increment this counter at each generation
    eoIncrementor<unsigned> increment(generationCounter.value());

    // add to the checkpoint to be processed, and monitor to be reported
    checkpoint.add(increment);

    // time counter
    eoTimeCounter timeStat;
    checkpoint.add(timeStat);

    // best fitness in population
    eoBestFitnessStat<Indi> bestStat("best");

    // add to the checkpoint to be processed, and monitor to be reported
    checkpoint.add(bestStat);

    // second moment stats: average and stdev
    eoSecondMomentStats<Indi> avgStdStat("avg std");
    checkpoint.add(avgStdStat);

    // report some statistics into the file
    monitor.add(generationCounter);  // iteration value
    monitor.add(timeStat);           // time report
    monitor.add(eval);               // number of evaluations
    monitor.add(bestStat);           // best fitness in the population
    monitor.add(avgStdStat);         // avg and std fitness in the population

    /* =========================================================
     *
     * the optimization algorithm
     *
     * ========================================================= */

    // a wonderfull Evolutionary Algorithm with HC local search inside
    eoEasyEA<Indi> solver(checkpoint, eval, select, variation, replace);

    /* =========================================================
     *
     * Run optimization algorithm
     *
     * ========================================================= */
	
    // create a pop of size mu using init for initialization
    eoPop<Indi> pop(mu, init);

	// evaluation of the initial population
    for(unsigned i = 0; i < pop.size(); i++)
        eval(pop[i]);

    // run the algorithm
    solver(pop);

    /* =========================================================
     *
     * Output
     *
     * ========================================================= */
	
    // sort the population according to fitness values (decreasing order)
    pop.sort();

    //std::cout << eval.value() << " " << neighborEval.value() << std::endl;
	std::cout << pop << std::endl;

}