//-----------------------------------------------------------------------------
/** 
 *
 * rndInit_fitness.cpp
 *
 * SV - 2021/11/28 - version 1
 *
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// the general include for eo

#include <eo>  // Evolving Object: fundation of EO, evolutionary algorithm
#include <mo>  // Moving   Object: Local search, combinatorial optimization mainly

//-----------------------------------------------------------------------------
// Fitness function

#include <eval/oneMaxEval.h> // oneMax problem from problems/eval/oneMaxEval.h

//-----------------------------------------------------------------------------
// Representation: define your individuals

typedef double Fitness;        // Fitness type, maximization problem
typedef eoBit<Fitness> Indi;   // bit string with double fitness value


int main(int argc, char** argv) {
    /* =========================================================
     *
     * Parameters
     *
     * ========================================================= */

    eoParser parser(argc, argv);

  	// random seed
    uint32_t seed = parser.createParam(time(0), "seed", "Random number seed", 'S', "Input").value();

    // bit string size
    size_t n = parser.createParam<size_t>(20, "vecSize", "Problem dimension", 'n', "Problem").value();

    make_verbose(parser);
    make_help(parser);

    /* =========================================================
     *
     * Fitness function
     *
     * ========================================================= */

    // pre-define fitness function to maximize: oneMax problem, i.e. number of 1
    oneMaxEval<Indi> eval;

    /* =========================================================
     *
     * Random seed
     *
     * ========================================================= */

    rng.reseed(seed);

    /* =========================================================
     *
     * Initialization
     *
     * ========================================================= */

    // initialisation of one single gene (bit)
    eoUniformGenerator<Indi::AtomType> genInit;

    // Initialisation of the solution (vector of bits)
    eoInitFixedLength<Indi> init(n, genInit);

    /* =========================================================
     *
     * Run optimization algorithm
     *
     * ========================================================= */
	
	// declaration of the solution
	Indi solution(n);

	// initialization of the solution
	init(solution);

	// evaluation of the solution
	eval(solution);

    /* =========================================================
     *
     * Output
     *
     * ========================================================= */
	
	std::cout << solution << std::endl;

}