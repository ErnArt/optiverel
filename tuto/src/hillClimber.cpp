//-----------------------------------------------------------------------------
/** 
 *
 * hillClimber.cpp
 *
 * SV - 2021/11/28 - version 1
 *
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// the general include for eo

#include <eo>  // Evolving Object: core of EO, evolutionary algorithm
#include <mo>  // Moving   Object: Local search, combinatorial optimization mainly

//-----------------------------------------------------------------------------
// Fitness function
#include <eval/nkLandscapesEval.h> // nk landscapes problem (from problesm/eval/nkLandscapesEval.h)
#include <problems/eval/moNKlandscapesIncrEval.h> // evaluation of a neighbor for nk-landscapes problem (from mo/src/problems/eval/moNKlandscapesIncrEval.h)

//-----------------------------------------------------------------------------
// type of the representations
typedef double Fitness;              // Fitness type, maximization problem
typedef eoBit<Fitness> Indi;         // bit string with double fitness value
typedef moBitNeighbor<Fitness> Neighbor; // neighbor: one bit flip


int main(int argc, char** argv) {
    /* =========================================================
     *
     * Parameters
     *
     * ========================================================= */

    eoParser parser(argc, argv);

  	// random seed
    uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();

    // bit string size
    size_t n = parser.createParam<size_t>(20, "vecSize", "Problem dimension", 'n', "Problem").value();

    // non-linear degree (K)
    size_t k = parser.createParam<size_t>(1, "nonLinearDegree", "Non-linear degree of nk-landscapes", 'k', "Problem").value();

    // random seed of nk-landscape
    uint32_t seedNK = parser.createParam(time(0), "seedNK", "Random number seed of the nk-landscape instance", 'I', "Problem").value();

    make_verbose(parser);
    make_help(parser);

    /* =========================================================
     *
     * Fitness function
     *
     * ========================================================= */

    // pre-define fitness function to maximize: nk-landscapes problem
    rng.reseed(seedNK);
    nkLandscapesEval<Indi> eval(n, k);

    // Use it if there is no incremental evaluation: a neighbor is evaluated by the full evaluation of a solution
    //moFullEvalByModif<Neighbor> neighborEval(eval);

    // Incremental evaluation of the neighbor
    moNKlandscapesIncrEval<Neighbor> neighborEval(eval);

    /* =========================================================
     *
     * Random seed
     *
     * ========================================================= */

    rng.reseed(seed);

    /* =========================================================
     *
     * Initialization
     *
     * ========================================================= */

    // initialisation of one single gene (bit)
    eoUniformGenerator<Indi::AtomType> genInit;

    // Initialisation of the solution (vector of bits)
    eoInitFixedLength<Indi> init(n, genInit);

    /* =========================================================
     *
     * Neighborhood
     *
     * ========================================================= */

    // Exploration of the neighborhood in random order of the neighbor's index:
    // each neighbor is visited only once
    moRndWithoutReplNeighborhood<Neighbor> neighborhood(n);

    /* =========================================================
     *
     * the local search algorithm
     *
     * ========================================================= */

    // hill-climber local search with first-improvement pivot rule
    moFirstImprHC<Neighbor> solver(neighborhood, eval, neighborEval);

    /* =========================================================
     *
     * Run optimization algorithm
     *
     * ========================================================= */
	
	// declaration of the solution
	Indi solution(n);

	// initialization of the solution
	init(solution);

	// evaluation of the solution
	eval(solution);

    // run the algorithm
    solver(solution);

    /* =========================================================
     *
     * Output
     *
     * ========================================================= */
	
	std::cout << solution << std::endl;

}