//-----------------------------------------------------------------------------
/** 
 *
 * hellEO.cpp
 *
 * SV - 2021/11/28 - version 1
 *
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// the general include for paradisEO

#include <eo>    // Evolving Object: core of EO, evolutionary algorithm
#include <ga.h>  // Genetic Algorithm part: bit strings search space

//-----------------------------------------------------------------------------
// Representation: define your individuals

typedef double Fitness;      // Fitness type
typedef eoBit<Fitness> Indi; // Type of object to evolve


int main(int argc, char** argv) {
	// solution of 20 bits length 
	Indi solution(20);

	// Print it, but without a valid fitness value.
	std::cout << solution << std::endl;
}