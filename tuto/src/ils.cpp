//-----------------------------------------------------------------------------
/** 
 *
 * ils.cpp
 *
 * SV - 2021/11/29 - version 1
 *
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// the general include for eo

#include <eo>   // Evolving Object: fundation of EO, evolutionary algorithm
#include <mo>   // Moving   Object: Local search, combinatorial optimization mainly
#include <ga.h> // Genetic (binary strings search space) algorithm part of EO 

//-----------------------------------------------------------------------------
// Fitness function
#include <eval/quadraticEval2.h> // nk landscapes problem (from problesm/eval/nkLandscapesEval.h)
#include <eval/nkLandscapesEval.h> // nk landscapes problem (from problesm/eval/nkLandscapesEval.h)
#include <problems/eval/moNKlandscapesIncrEval.h> // evaluation of a neighbor for nk-landscapes problem (from mo/src/problems/eval/moNKlandscapesIncrEval.h)

//-----------------------------------------------------------------------------
// type of the representations
typedef double Fitness;              // Fitness type, maximization problem
typedef eoBit<Fitness> Indi;         // bit string with double fitness value
typedef moBitNeighbor<Fitness> Neighbor; // neighbor: one bit flip


int main(int argc, char** argv) {
    /* =========================================================
     *
     * Parameters
     *
     * ========================================================= */

    eoParser parser(argc, argv);

  	// random seed
    uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();

    // bit string size
    size_t n = parser.createParam<size_t>(20, "vecSize", "Problem dimension", 'n', "Problem").value();

    // non-linear degree (K)
    size_t k = parser.createParam<size_t>(1, "nonLinearDegree", "Non-linear degree of nk-landscapes", 'k', "Problem").value();

    // random seed of nk-landscape
    uint32_t seedNK = parser.createParam(time(0), "seedNK", "Random number seed of the nk-landscape instance", 'I', "Problem").value();

    // Neighborhood selection strategy
    unsigned int selectionStrategy = parser.createParam(0, "pivot", "Pivot rule of the hill-climber. 0: best improvement, 1: first improvement", 'p', "Algorithm").value();

    // pertubation : bit flip mutation
    double mutationRate_perBit = parser.createParam(5.0 / n, "mrate", "Mutation rate per bit", 'm', "Algorithm").value();

    // stopping criterium based on time 
    time_t duration = parser.createParam(1, "time", "Time limit stopping criterium (number of seconds)", 't', "Algorithm").value();

    // stopping criterium based on number of evaluations
    uint32_t maxEval = parser.createParam(1000, "maxEval", "Number of evaluations stopping criterium", 'e', "Algorithm").value();

    make_verbose(parser);
    make_help(parser);

    /* =========================================================
     *
     * Fitness function
     *
     * ========================================================= */

    // pre-define fitness function to maximize: nk-landscapes problem
    rng.reseed(seedNK);
    quadraticEval2<Indi> _eval;
    // to count the number of evalation
    eoEvalFuncCounter<Indi> eval(_eval);

    // Use it if there is no incremental evaluation: a neighbor is evaluated by the full evaluation of a solution
    moFullEvalByModif<Neighbor> _neighborEval(_eval);

    // Incremental evaluation of the neighbor
    // moNKlandscapesIncrEval<Neighbor> _neighborEval(_eval);
    // to count the number of evalation
    moEvalCounter<Neighbor> neighborEval(_neighborEval);

    /* =========================================================
     *
     * Random seed
     *
     * ========================================================= */

    rng.reseed(seed);

    /* =========================================================
     *
     * Initialization
     *
     * ========================================================= */

    // initialisation of one single gene (bit)
    eoUniformGenerator<Indi::AtomType> genInit;

    // Initialisation of the solution (vector of bits)
    eoInitFixedLength<Indi> init(n, genInit);

    /* =========================================================
     *
     * Neighborhood
     *
     * ========================================================= */

    // Exploration of the neighborhood in random order of the neigbor's index:
    // each neighbor is visited only once
    moRndWithoutReplNeighborhood<Neighbor> rndNeighborhood(n);

    moOrderNeighborhood<Neighbor> orderNeighborhood(n);

    /* =========================================================
     *
     * continuator: stopping criterium
     *
     * ========================================================= */

    // time stopping criterium
    moTimeContinuator<Neighbor> timeContinuator(duration, false);

    // maximum number of evaluations
    moEvalsContinuator<Neighbor> evalContiuator(eval, neighborEval, maxEval, false);

    // multiple stopping criteria
    moCombinedContinuator<Neighbor> continuator(timeContinuator);
    continuator.add(evalContiuator);

    /* =========================================================
     *
     * basic local search
     *
     * ========================================================= */

    moLocalSearch<Neighbor> * hc;

    switch (selectionStrategy) {
        // best improvement
        case 0: hc = new moSimpleHC<Neighbor>(orderNeighborhood, eval, neighborEval, continuator);
        break;
        // first improvement
        case 1: hc = new moFirstImprHC<Neighbor>(rndNeighborhood, eval, neighborEval, continuator);
        break;
    }

    /* =========================================================
     *
     * Pertubation
     *
     * ========================================================= */

    // Perturbation: standard bit-flip mutation for bitstring
    eoBitMutation<Indi>  mutation(mutationRate_perBit);

    /* =========================================================
     *
     * the local search algorithm
     *
     * ========================================================= */

    moILS<Neighbor, Neighbor> solver(*hc, eval, mutation, continuator);

    /* =========================================================
     *
     * Run optimization algorithm
     *
     * ========================================================= */
	
	// declaration of the solution
	Indi solution(n);

	// initialization of the solution
	init(solution);

	// evaluation of the solution
	eval(solution);

    // run the algorithm
    solver(solution);

    /* =========================================================
     *
     * Output
     *
     * ========================================================= */
	
    std::cout << eval.value() << " " << neighborEval.value() << std::endl;
	std::cout << solution << std::endl;

}