#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "SolutionSet.hpp"

using namespace std;

int main(int argc, char* argv[]) {
	if (argc < 2 or argc > 2) {
		cout<<"Not enough argument, you need to have :"<<endl;
		cout<<"./main fileName.csv"<<endl;
		return -1;
	}

	SolutionSet solutionSet(argv[1]);

	if (!solutionSet.isSolutionSetValid()) {
		cout<<"File error"<<endl;
		return -1;
	}

	SolutionSet nonDominatedSolution = solutionSet.getNonDominatedSolution();

	cout<<nonDominatedSolution.getSize()<<endl;

	return 0;
}

