#include "SolutionSet.hpp"

SolutionSet::SolutionSet(std::string fileName) {
	solutionVector = readCSV(fileName);
}

SolutionSet::SolutionSet(std::vector<Solution> valueVector) {
	solutionVector = valueVector;
}

std::vector<Solution> SolutionSet::readCSV(std::string fileName){

	std::vector<Solution> solutionVector;
	std::ifstream file(fileName);
	std::string line;

	if(!file.is_open()) {
		return solutionVector;
	}

	while(std::getline(file, line))
		solutionVector.push_back(Solution(line));

	file.close();

	return solutionVector;
}

SolutionSet SolutionSet::getNonDominatedSolution() {
	std::vector<Solution> nonDominatedSolutionVector;
	for (unsigned i=0; i<solutionVector.size(); i++) {
		Solution currentSolution = solutionVector.at(i);
		bool currentSolutionNotDominated = true;
		for (unsigned j=0; j<solutionVector.size(); j++) {
			if (currentSolution.isCurrentSolutionDominated(solutionVector.at(j))) {
				currentSolutionNotDominated = false;
				break;
			}
		}
		if (currentSolutionNotDominated)
			nonDominatedSolutionVector.push_back(currentSolution);
	}

	return SolutionSet(nonDominatedSolutionVector);
}

int SolutionSet::getSize() {
	return solutionVector.size();
}

bool SolutionSet::isSolutionSetValid() {
	return solutionVector.size() != 0;
}

void SolutionSet::printSolutionSet() {
	for (int i=0; i<solutionVector.size(); i++)
		solutionVector.at(i).printSolution();
}
