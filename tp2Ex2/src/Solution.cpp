#include "Solution.hpp"

Solution::Solution(std::string fileName) {
	valueVector = readLine(fileName);
}

std::vector<double> Solution::readLine(std::string line){

	std::vector<double> value;
	std::stringstream lineStream(line);
	double number;
	
	while (lineStream >> number)
		value.push_back(number);

	return value;
}

std::vector<double> Solution::getValues() {
	return valueVector;
}

bool Solution::isSolutionValid() {
	return valueVector.size() != 0;
}

bool Solution::isCurrentSolutionDominated(Solution solution) {
	unsigned i=0;
	bool ok=false;

	std::vector<double> solutionValueVector = solution.getValues();

	while(i<valueVector.size() and valueVector.at(i) <= solutionValueVector.at(i)) {
		ok = ok or (valueVector.at(i) < solutionValueVector.at(i));
		i++;
	}

	return (i == valueVector.size() and ok);;

	/*
	for(int i=0; i<valueVector.size(); i++) {
		if ()
	}
	*/
}

void Solution::printSolution() {
	for (int i=0; i<valueVector.size(); i++)
		std::cout<<valueVector.at(i)<<" ";
	std::cout<<std::endl;
}
