#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

class Solution {
	private:
		std::vector<double> valueVector;

	public:
		Solution(std::string fileName);
		std::vector<double> readLine(std::string fileName);
		std::vector<double> getValues();
		bool isCurrentSolutionDominated(Solution solution);
		bool isSolutionValid();
		void printSolution();
};