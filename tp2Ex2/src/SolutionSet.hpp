#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "Solution.hpp"

class SolutionSet {
	private:
		std::vector<Solution> solutionVector;

	public:
		SolutionSet(std::string fileName);
		SolutionSet(std::vector<Solution> valueVector);
		std::vector<Solution> readCSV(std::string fileName);
		SolutionSet getNonDominatedSolution();
		int getSize();
		bool isSolutionSetValid();
		void printSolutionSet();
};