import pandas as pd

def open_file():
	data = pd.read_csv("../../build/out.csv", sep=" ")
	return data

def main():
	data = open_file()
	print(data)

if __name__ == "__main__":
	main()