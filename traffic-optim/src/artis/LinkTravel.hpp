/**
 * @file LinkTravel.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP4_LINK_TRAVEL_HPP
#define TP4_LINK_TRAVEL_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include "Base.hpp"
#include "Vehicle.hpp"

namespace tp4 {

struct LinkTravelParameters
{
    double length;
    unsigned int lane_number;
    double free_speed;
    double concentration;
    double wave_speed;
    double capacity;
    std::deque<std::pair<double, double>> modified_capacities;
    int id_link;
};

class LinkTravel
        : public artis::pdevs::Dynamics<artis::common::DoubleTime, LinkTravel, LinkTravelParameters>
{
public:
    struct inputs
    {
        enum values
        {
            IN, IN_CONFIRM_OUT
        };
    };

    struct outputs
    {
        enum values
        {
            OUT, OUT_OPEN, OUT_CLOSE
        };
    };

    struct vars
    {
        enum values
        {
            VEHICLE_NUMBER
        };
    };

    LinkTravel(const std::string &name,
         const artis::pdevs::Context<artis::common::DoubleTime, LinkTravel, LinkTravelParameters> &context)
            :
            artis::pdevs::Dynamics<artis::common::DoubleTime, LinkTravel, LinkTravelParameters>(name, context),
            _length(context.parameters().length),
            _lane_number(context.parameters().lane_number),
            _free_speed(context.parameters().free_speed),
            _concentration(context.parameters().concentration),
            _wave_speed(context.parameters().wave_speed),
            _capacity(context.parameters().capacity),
            _modified_capacities(context.parameters().modified_capacities),
            _id_link(context.parameters().id_link)
    {
        input_ports({{inputs::IN, "in"},
                     {inputs::IN_CONFIRM_OUT, "in_confirm_out"}});
        output_ports({
                             {outputs::OUT, "out"},
                             {outputs::OUT_OPEN, "out_open"},
                             {outputs::OUT_CLOSE, "out_close"}
                     });
        observables({{vars::VEHICLE_NUMBER, "vehicle_number"}});
    }

    ~LinkTravel() override = default;

    void dconf(
            const Time & /* t* */,
            const Time & /* e */,
            const Bag & /* bag */) override;

    void dint(const Time & /* t */) override;

    void dext(
            const Time & /* t */,
            const Time & /* e */,
            const Bag & /* bag*/) override;

    void start(const Time & /* t */) override;

    Time ta(const Time & /* t */) const override;

    Bag lambda(const Time & /* t */) const override;

    artis::common::Value observe(const Time &t,
                                 unsigned int index) const override;

private:
    void update_vehicle_state(const Time &t);
    void update_open_close_state(const Time &t);
    void update_sigma(const Time &t);
    void update_capacity(const Time &t);
    bool vehicle_ready(const Time &t) const;

    struct Entry
    {
        Vehicle vehicle;
        Time in_time;
        Time next_time;
    };

    struct OpenClosePhase
    {
        enum values
        {
            OPENED, SEND_OPEN, SEND_CLOSE, CLOSED
        };

        static std::string to_string(const values &value)
        {
            switch (value) {
                case OPENED:return "OPENED";
                case SEND_OPEN:return "SEND_OPEN";
                case SEND_CLOSE:return "SEND_CLOSE";
                case CLOSED:return "CLOSED";
            }
            return "";
        }
    };

    struct Phase
    {
        enum values
        {
            WAIT, SEND
        };

        static std::string to_string(const values &value)
        {
            switch (value) {
                case WAIT:return "WAIT";
                case SEND:return "SEND";
            }
            return "";
        }
    };

    // parameters
    double _length;
    unsigned int _lane_number;
    double _free_speed;
    double _concentration;
    double _wave_speed;
    double _capacity;
    std::deque<std::pair<double, double>> _modified_capacities;
    int _id_link;

    // state
    std::deque<Entry> _vehicles;
    unsigned int _total_vehicle_number;
    unsigned int _vehicle_number;
    std::deque<std::pair<unsigned int, Time>> _delays;
    Time _sigma;
    Time _open_sigma;
    Time _modify_sigma;
    OpenClosePhase::values _open_close_phase;
    Phase::values _phase;
    Time _last_time;
};

}

#endif