/**
 * @file Vehicle.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP4_VEHICLE_HPP
#define TP4_VEHICLE_HPP

#include "Base.hpp"

#include <vector>

namespace tp4 {

typedef std::vector<unsigned int> Path;

struct Vehicle
{
  unsigned int index;
  double length;
  double speed;
  int64_t path;
  int8_t path_index;
  Time t_start;
  double consumption;

  int link;
  unsigned int number_on_link;
  Time t_entry;
  Time t_estimated_end;
  Time t_real_end;

  Vehicle() = default;

  Vehicle(unsigned int index, double length, double speed, Path path, const Time& start)
      : index(index), length(length), speed(speed), path_index(0), t_start(start), consumption(0)
  {
    this->path = 0;
    for (unsigned int p: path) {
      this->path <<= 1;
      this->path |= p;
    }
  }

  std::string to_string() const
  {
    return "VEHICLE[ " + std::to_string(index)
        + " ; " + std::to_string(length)
        + " ; " + std::to_string(speed)
        + " ]";
  }

  std::string getJson() const
  {
      return "{ \"id\": " + std::to_string(index) + ","
             + "\"link\": " + std::to_string(link) + ","
             + "\"t_entry\": " + std::to_string(t_entry) + ","
             + "\"t_estimated_end\": " + std::to_string(t_estimated_end) + ","
             + "\"t_real_end\": " + std::to_string(t_real_end) + " }";
  }
};

struct Data
{
  unsigned int index;
  double open_time;
};

}

#endif