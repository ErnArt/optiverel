/**
 * @file LinkTravel.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LinkTravel.hpp"

#include <cmath>

namespace tp4 {

void LinkTravel::dconf(const Time &t,
                 const Time &/* e */,
                 const Bag &bag)
{
  dint(t);
  dext(t, 0, bag);
}

void LinkTravel::dint(const Time &t)
{
  double e = std::min(_sigma, std::min(_open_sigma, _modify_sigma));

  if (_sigma != artis::common::DoubleTime::infinity) {
    _sigma -= e;
  }
  if (_open_sigma != artis::common::DoubleTime::infinity) {
    _open_sigma -= e;
  }
  if (_modify_sigma != artis::common::DoubleTime::infinity) {
    _modify_sigma -= e;
  }
  update_vehicle_state(t);
  update_sigma(t);
  update_open_close_state(t);
  update_capacity(t);
  _last_time = t;
}

void LinkTravel::dext(const Time &t,
                const Time &e,
                const Bag &bag)
{
  if (_sigma != artis::common::DoubleTime::infinity) {
    _sigma -= e;
  }
  if (_open_sigma != artis::common::DoubleTime::infinity) {
    _open_sigma -= e;
  }
  if (_modify_sigma != artis::common::DoubleTime::infinity) {
    _modify_sigma -= e;
  }
  while (!_delays.empty() && _delays.front().second <= t) {
    _delays.pop_front();
  }

  std::for_each(bag.begin(), bag.end(),
                [t, this](const ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {
                      Vehicle vehicle{};

                      assert((_open_close_phase != OpenClosePhase::CLOSED
                              and _open_close_phase != OpenClosePhase::SEND_CLOSE)
                             or _open_sigma < 1e-6);

                      event.data()(vehicle);

                      assert(_vehicles.empty() or (not _vehicles.empty()
                                                   and t - _vehicles.back().in_time - 1. / _capacity > -1e-6));

                      ++_vehicle_number;
                      ++_total_vehicle_number;

                      double out_time = t + _length / _free_speed;
                      vehicle.link = _id_link;
                      vehicle.t_entry = t;
                      vehicle.t_estimated_end = out_time;
                      vehicle.number_on_link = _total_vehicle_number;
                      _vehicles.push_back(Entry{vehicle, t, out_time});

                      _open_close_phase = OpenClosePhase::SEND_CLOSE;
                      if(_vehicle_number >= (unsigned int) (_concentration * _length)){
                        _open_sigma = artis::common::DoubleTime::infinity;
                      } else if (!_delays.empty() && _delays.front().first == _total_vehicle_number + 1 &&
                                 _delays.front().second > t) {
                        _open_sigma = std::max(_delays.front().second - t, 1. / _capacity);
                        _delays.pop_front();
                      } else {
                        _open_sigma = 1. / _capacity;
                      }
                      _sigma = 0;
                  } else if (event.on_port(inputs::IN_CONFIRM_OUT)) {
                    Vehicle vehicle{};
                    event.data()(vehicle);

                    if(vehicle.t_estimated_end < vehicle.t_real_end) {
                      unsigned int dn = std::ceil(_length * _concentration);
                      double dt = std::ceil(_length / _wave_speed);
                      unsigned int dn_real = vehicle.number_on_link + dn;
                      double dt_real = t + dt;
                      _delays.push_back({dn_real, dt_real});
                    }
                    --_vehicle_number;
                    if(_vehicle_number <= (unsigned int) (_concentration * _length) &&
                       _open_close_phase == OpenClosePhase::CLOSED &&
                       _open_sigma == artis::common::DoubleTime::infinity) {
                      if(!_delays.empty() && _delays.front().first == _total_vehicle_number + 1 &&
                         _delays.front().second <= t){
                        _open_sigma = std::max(0.,
                                               std::max(_delays.front().second - t,
                                                        1. / _capacity - (t - _vehicles.back().in_time)));
                        _delays.pop_front();
                      } else {
                        _open_sigma = std::max(0., 1. / _capacity - (t - _vehicles.back().in_time));
                      }
                    }
                  }
                });
  _last_time = t;
}

void LinkTravel::start(const Time &t)
{
  _vehicle_number = 0;
  _total_vehicle_number = 0;
  _open_sigma = 0;
  _open_close_phase = OpenClosePhase::SEND_OPEN;
  _phase = Phase::WAIT;
  _sigma = artis::common::DoubleTime::infinity;
  _modify_sigma = _modified_capacities.empty() ? artis::common::DoubleTime::infinity : _modified_capacities.front().first - t;
  _last_time = t;
}

Time LinkTravel::ta(const Time & /* t */) const
{
  return std::min(_sigma, std::min(_open_sigma, _modify_sigma));
}

Bag LinkTravel::lambda(const Time &t) const
{
  Bag bag;

  if (_phase == Phase::SEND) {
    Vehicle vehicle = _vehicles.front().vehicle;

    vehicle.consumption += 3. + std::exp(_free_speed / 16) / 100 * _length * 2.4;
    bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT, vehicle));
  }
  if (_open_close_phase == OpenClosePhase::SEND_OPEN) {
    bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT_OPEN));
  } else if (_open_close_phase == OpenClosePhase::SEND_CLOSE) {
    bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT_CLOSE,
                                                                          (double) (t + _open_sigma)));
  }
  return bag;
}

artis::common::Value LinkTravel::observe(const Time & /* t */,
                                   unsigned int index) const
{
  switch (index) {
  case vars::VEHICLE_NUMBER:return (unsigned int) _vehicles.size();
  default:return artis::common::Value();
  }
}

void LinkTravel::update_vehicle_state(const Time &t){
  if (_phase == Phase::WAIT and vehicle_ready(t)) {
    _phase = Phase::SEND;
    _sigma = 0;
  }else if(_phase == Phase::SEND){
    _phase = Phase::WAIT;
    _vehicles.pop_front();
  }
}

void LinkTravel::update_open_close_state(const Time & /* t */)
{
  if (_open_close_phase == OpenClosePhase::SEND_OPEN) {
    _open_close_phase = OpenClosePhase::OPENED;
    _open_sigma = artis::common::DoubleTime::infinity;
  } else if (_open_close_phase == OpenClosePhase::SEND_CLOSE) {
    if (_open_sigma > 0) {
      _open_close_phase = OpenClosePhase::CLOSED;
    } else {
      _open_close_phase = OpenClosePhase::OPENED;
      _open_sigma = artis::common::DoubleTime::infinity;
    }
  } else if (_open_close_phase == OpenClosePhase::CLOSED and _open_sigma < 1e-6) {
    _open_close_phase = OpenClosePhase::SEND_OPEN;
    _open_sigma = 0;
  }
}

void LinkTravel::update_sigma(const Time &t)
{
  if (_vehicles.empty()) {
    _sigma = artis::common::DoubleTime::infinity;
  } else {
    _sigma = _vehicles.front().next_time - t;
  }
}

void LinkTravel::update_capacity(const Time &t)
{
  if (_modify_sigma < 1e-6) {
    _capacity = _modified_capacities.front().second;
    _modified_capacities.pop_front();
    if (!_modified_capacities.empty()) {
      _modify_sigma = _modified_capacities.front().first - t;
    } else {
      _modify_sigma = artis::common::DoubleTime::infinity;
    }
    if (_vehicles.back().in_time + 1. / _capacity > t) {
      _open_close_phase = OpenClosePhase::SEND_CLOSE;
      _open_sigma = _vehicles.back().in_time + 1. / _capacity - t;
      _sigma = 0;
    }
  }
}

bool LinkTravel::vehicle_ready(const Time &t) const
{
    return (not _vehicles.empty() and std::abs(_vehicles.front().next_time - t) < 1e-6);
}

}