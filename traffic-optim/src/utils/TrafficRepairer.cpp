#ifndef _TrafficBounder_cpp
#define _TrafficBounder_cpp

#include <edoRepairer.h>
#include <eo>

template <typename EOT> class TrafficRepairer : public edoRepairer<EOT>
{
	private:
		eoRealVectorBounds bounds;

	public:
		TrafficRepairer(eoRealVectorBounds bounds): edoRepairer<EOT>(), bounds(bounds) {}

		void operator() (EOT & solution) {
			for (unsigned i = 0; i<solution.size(); i++)
				if (solution[i] < bounds[i] -> minimum() or solution[i] > bounds[i] -> maximum())
					solution[i] = rng.uniform(bounds[i] -> minimum(), bounds[i] -> maximum());
		}
};
#endif
