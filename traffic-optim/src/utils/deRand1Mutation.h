#ifndef _DERand1Mutation_h
#define _DERand1Mutation_h

#include <vector>
#include <utility>
#include "deOp.h"

/** 

*/
template <class EOT>
class DERand1Mutation : public DEOp<EOT>
{
  using DEOp<EOT>::parents;
  using DEOp<EOT>::id;

public:
    DERand1Mutation(eoPop<EOT> & _parents, double _F) : DEOp<EOT>(_parents), F(_F)
    {
    }

    /*
      Mutation of the solution
    */
    virtual bool operator()(EOT & _solution) {
      if (parents.size() > 4) {
        unsigned k, i1, i2, i3;

        // random different indices
        std::vector<unsigned> indexes(parents.size());
        for(unsigned i = 0; i < indexes.size(); i++)
          indexes[i] = i;

        unsigned lastId = indexes.size() - 1;
        std::swap(indexes[id], indexes[lastId]);  // remove id
        lastId--;

        k = rng.random(lastId + 1);
        i1 = indexes[k];
        std::swap(indexes[k], indexes[lastId]);  
        lastId--;

        k = rng.random(lastId + 1);
        i2 = indexes[k];
        std::swap(indexes[k], indexes[lastId]);  
        lastId--;

        k = rng.random(lastId + 1);
        i3 = indexes[k];
        std::swap(indexes[k], indexes[lastId]);  
        lastId--;

        // mutation
        _solution.resize(parents[id].size());

        for(unsigned j = 0; j < _solution.size(); j++)
          _solution[j] = parents[i1][j] + F * (parents[i2][j] - parents[i3][j]);

        _solution.invalidate();

        return true;
      }
    }

protected:
  double F;

};

#endif
