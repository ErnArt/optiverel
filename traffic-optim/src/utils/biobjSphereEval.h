/*
    <biobjSphereEval.h>

    Author: Verel Sébastien
    Date: 2022/01/04 V0
*/

#ifndef _biobjSphereEval_h
#define _biobjSphereEval_h

#include <core/moeoEvalFunc.h>

/**
 * Full evaluation Function 
 */
template< class EOT >
class BiobjSphereEval : public moeoEvalFunc<EOT>
{
public:

	/**
	 * Sphere fitness function for the 2 objective
     *
	 * @param _sol the solution to evaluate
	 */
    void operator() (EOT& _sol) {
        double d1 = 0;
        double d2 = 0;
       
        for (unsigned int i = 0; i < _sol.size() - 1; i++) {
            d1 += _sol[i] * _sol[i];
            d2 += (_sol[i] - 1.0) * (_sol[i] - 1.0);
        }
       
        typename EOT::ObjectiveVector objVec ;
        objVec[0] = d1;
        objVec[1] = d2;

        _sol.objectiveVector(objVec);
    }
};

#endif
