/*
*  archive.cpp 
*
* Author:
* SV - 2022/01/04 - version 1
*
*
*/
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// the general include for eo

#include <eo>   // Evolving Object: fundation of EO, evolutionary algorithm
#include <moeo>  // Evolving Multiobjective Object
#include <es.h>

//-----------------------------------------------------------------------------
// Fitness function
#include "utils/biobjSphereEval.h" // sphere function

//-----------------------------------------------------------------------------
// type of the representations

// type of objective vector
typedef moeoRealObjectiveVector< moeoObjectiveVectorTraits > ObjectiveVector;

// type of solution
typedef moeoRealVector<ObjectiveVector> Indi;         // vector of double with vector of double for objective

int main(int argc, char** argv) {
    /* =========================================================
     *
     * Parameters
     *
     * ========================================================= */

    eoParser parser(argc, argv);

    // random seed
    uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();

    // problem dimension
    size_t d = parser.createParam<size_t>(10, "vecSize", "Problem dimension", 'd', "Problem").value();

    // Population size
    uint32_t mu = parser.createParam(200, "popSize", "Population size", 'P', "Algorithm").value();

    make_verbose(parser);
    make_help(parser);

    /* =========================================================
     *
     * Fitness function: objective vector
     *
     * ========================================================= */

    // set the objective vector: false:maximization, true:minimization
    // two objectives to minimize (true for each objective)
    std::vector<bool> objVec(2, true);
    moeoObjectiveVectorTraits::setup(2, objVec);

    BiobjSphereEval<Indi> eval;

    /* =========================================================
     *
     * Random seed
     *
     * ========================================================= */

    rng.reseed(seed);

    /* =========================================================
     *
     * Initialization
     *
     * ========================================================= */

    std::vector<double> lowerBounds(d, -5.0);
    std::vector<double> upperBounds(d,  5.0);
    eoRealVectorBounds bounds(lowerBounds, upperBounds);

    eoRealInitBounded<Indi> init(bounds);

    /* =========================================================
     *
     * Random solution
     *
     * ========================================================= */
    
    // create a pop of size mu using init for initialization
    eoPop<Indi> pop(mu, init);

    // evaluation of the initial population
    for(unsigned i = 0; i < pop.size(); i++)
        eval(pop[i]);

    /* =========================================================
     *
     * Archive 
     *
     * ========================================================= */
    
    // archive of with non-dominated solutions
    moeoUnboundedArchive<Indi> archive;

    // put in archive (filter dominance)
    for(unsigned i = 0; i < pop.size(); i++)
        archive(pop[i]);

    /* =========================================================
     *
     * Output
     *
     * ========================================================= */
    
    // print the whole population
    //std::cout << pop << std::endl;

    //std::cout << "Set of non dominated solutions:" << std::endl;
    std::cout << archive << std::endl;
}